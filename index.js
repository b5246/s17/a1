let studname = [];
let studNameSort = [];
let studFilter;
function addStudent(name){
	studname.push(name);
	console.log(name + " was added to the student list.");
}
function countStudents(){
	console.log("There are a total of "+studname.length+" student/s enrolled");
}

function printStudents(){
	studNameSort = studname.sort();
	console.log(studNameSort.join('\n'))
}
function findStudent(refer){
	let refArr= studname.map(function(number){return number.toLowerCase();});
	studFilter = refArr.filter(function(name){return name.includes(refer.toLowerCase())});
	let arrIndex = studname.filter(function(name){return name.indexOf(studFilter)});
	
	if (studFilter.length == 1){
		let singleName = refArr.indexOf(studFilter.join(""));
		console.log(studname[singleName] +" is an Enrollee.");
	}else if(studFilter.length > 1){
		console.log(arrIndex.join(',') +" are Enrollees.");
	}else if(studFilter.length == 0){
		console.log("No student found with the name "+ refer );
	}
}

